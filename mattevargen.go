// go:build js/wasm
package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"syscall/js"
	"time"
)

const maxInt = 20
var answer int

func randInt(from int, to int) int {
	return rand.Intn(to-from+1) + from
}

func mathLoad() (int, int, string) {
	var sign = randInt(1, 2)
	var a, b int
	var signStr string

	if sign == 1 {  // addition
		a = randInt(1, maxInt)
		b = randInt(1, maxInt)
		answer = a + b
		signStr = "+"
	} else if sign == 2 {
		a = randInt(1, maxInt)
		b = randInt(1, a)
		answer = a - b
		signStr = "-"
	}

	return a, b, signStr;	
}

func mathLoadJS() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) any {
		jsDoc := js.Global().Get("document")

		answerElement := jsDoc.Call("getElementById", "answer")
		classList := answerElement.Get("classList")
		classList.Call("remove", "is-valid")
		classList.Call("remove", "is-invalid")
		answerElement.Set("value", "")

		var a, b, signStr = mathLoad()
		
		problemElement := jsDoc.Call("getElementById", "problem")
		problemElement.Set("textContent", fmt.Sprintf("%d %s %d =", a, signStr, b))
		return nil
	})
}

func mathAnswerJS() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) any {
		jsDoc := js.Global().Get("document")
		answerElement := jsDoc.Call("getElementById", "answer")
		givenAnswerStr := answerElement.Get("value").String()
		givenAnswer, err := strconv.Atoi(givenAnswerStr)

		classList := answerElement.Get("classList")
		if err == nil && givenAnswer == answer {
			classList.Call("add", "is-valid")
			classList.Call("remove", "is-invalid")
		} else {
			classList.Call("remove", "is-valid")
			classList.Call("add", "is-invalid")
		}        
		return nil
	})
}
	

func main() {
	rand.Seed(time.Now().UnixNano())
	fmt.Println("Mattevargen GO")
	js.Global().Set("mathLoad", mathLoadJS())
	js.Global().Set("mathAnswer", mathAnswerJS())
	<-make(chan struct{})
}
