SOURCE = mattevargen.go go.mod
TARGET = html/json.wasm

$(TARGET): $(SOURCE)
	GOOS=js GOARCH=wasm go build -o $(TARGET)

all: $(TARGET)

rsync:
	rsync -var --delete html/ lakka:www/matte/
